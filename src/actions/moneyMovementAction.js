import { ADD_MONEY_MOVEMENT } from '../constants/moneyMovements_action_types';

export const addMoneyMovement = (moneyMovement) => ({
	type: ADD_MONEY_MOVEMENT,
	payload: moneyMovement
});
