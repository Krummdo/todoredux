import { ADD_TODO, DONE_UNDONE_TODO, DELETE_TODO } from '../constants/todo_action_types';

export const addTodo = (todo) => ({
	type: ADD_TODO,
	payload: todo
});

export const doneUndoneTodo = (id) => ({
	type: DONE_UNDONE_TODO,
	payload: id
});

export const deleteTodo = (id) => ({
	type: DELETE_TODO,
	payload: id
});
