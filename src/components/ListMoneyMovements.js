import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = (theme) => ({
	root: {
		margin: theme.spacing.unit * 4,
		overflowX: 'auto'
	},
	table: {
		minWidth: 700
	}
});

function ccyFormat(num) {
	return `${parseFloat(num).toFixed(2)}`;
}

class ConnectedListMoneyMovements extends Component {
	render() {
		const { classes, moneyMovements } = this.props;
		const incomeSubtotal = this.props.moneyMovements.map(({ income }) => income).reduce((sum, i) => sum + i, 0);
		const outcomeSubtotal = this.props.moneyMovements.map(({ outcome }) => outcome).reduce((sum, i) => sum + i, 0);
		const total = parseFloat(incomeSubtotal) - ccyFormat(outcomeSubtotal);

		return (
			<Paper className={classes.root}>
				<Table className={classes.table}>
					<TableHead>
						<TableRow>
							<TableCell>Date</TableCell>
							<TableCell>Description</TableCell>
							<TableCell numeric>Income</TableCell>
							<TableCell numeric>Outcome</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{moneyMovements.map((moneyMovement) => (
							<TableRow key={moneyMovement.id}>
								<TableCell>{moneyMovement.selectedDateString}</TableCell>
								<TableCell>{moneyMovement.description}</TableCell>
								<TableCell numeric>{ccyFormat(moneyMovement.income)}</TableCell>
								<TableCell numeric>{ccyFormat(moneyMovement.outcome)}</TableCell>
							</TableRow>
						))}
						<TableRow>
							<TableCell rowSpan={3} />
							<TableCell>Subtotal</TableCell>
							<TableCell numeric>{ccyFormat(incomeSubtotal)}</TableCell>
							<TableCell numeric>{ccyFormat(outcomeSubtotal)}</TableCell>
						</TableRow>
						<TableRow>
							<TableCell colSpan={2}>Balance</TableCell>
							<TableCell numeric>{ccyFormat(total)}</TableCell>
						</TableRow>
					</TableBody>
				</Table>
			</Paper>
		);
	}
}

const mapStateToProps = (state) => {
	return { moneyMovements: state.moneyMovementsReducer.moneyMovements };
};

const ListMoneyMovements = connect(mapStateToProps)(ConnectedListMoneyMovements);

ListMoneyMovements.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ListMoneyMovements);
