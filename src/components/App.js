import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Grow from '@material-ui/core/Grow';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';
import ListTodo from './ListTodo';
import FormTodo from './FormTodo';
import ListMoneyMovements from './ListMoneyMovements';
import FormMoneyMovements from './FormMoneyMovements';

const styles = (theme) => ({
	root: {
		flexGrow: 1,
		padding: theme.spacing.unit * 3,
		marginLeft: theme.spacing.unit * 10,
		marginRight: theme.spacing.unit * 10
	},
	paper: {
		textAlign: 'center',
		color: theme.palette.text.secondary
	},
	titles: {
		paddingTop: theme.spacing.unit * 4
	}
});

class App extends Component {
	render() {
		const { classes } = this.props;
		return (
			<div className={classes.root}>
				<Slide direction="left" in timeout={500}>
					<Typography variant="h4" gutterBottom align="left">
						QUECUANDOHAGO
					</Typography>
				</Slide>
				<Grow in style={{ transformOrigin: '0 0 0' }} timeout={700}>
					<Paper className={classes.paper}>
						<Grid container spacing={8}>
							<Grid container item xs={12} spacing={8}>
								<Grid item xs={6}>
									<FormTodo />
								</Grid>
								<Grid item xs={6}>
									<ListTodo />
								</Grid>
							</Grid>
						</Grid>
					</Paper>
				</Grow>
				<Slide direction="left" in timeout={700}>
					<Typography className={classes.titles} variant="h4" gutterBottom align="left">
						QUECOMOGASTO
					</Typography>
				</Slide>
				<Grow in style={{ transformOrigin: '0 0 0' }} timeout={900}>
					<Paper className={classes.paper}>
						<Grid container spacing={8}>
							<Grid container item xs={12} spacing={8}>
								<Grid item xs={6}>
									<FormMoneyMovements />
								</Grid>
								<Grid item xs={6}>
									<ListMoneyMovements />
								</Grid>
							</Grid>
						</Grid>
					</Paper>
				</Grow>
			</div>
		);
	}
}

App.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(App);
