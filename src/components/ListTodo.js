import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { doneUndoneTodo, deleteTodo } from '../actions/todoAction';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

const styles = (theme) => ({
	root: {
		width: '100%',
		padding: theme.spacing.unit * 4
	},
	heading: {
		fontSize: theme.typography.pxToRem(15),
		marginRight: theme.spacing.unit,
		flexShrink: 0
	},
	secondaryHeading: {
		fontSize: theme.typography.pxToRem(15),
		color: theme.palette.text.secondary
	},
	checkboxHeading: {
		marginRight: theme.spacing.unit * 2,
		padding: 0
	},
	iconButtonHeading: {
		marginRight: theme.spacing.unit * 4,
		padding: 0
	},
	panelHover: {
		'&:hover': {
			background: '#E0E0E0'
		}
	}
});

class ConnectedList extends Component {
	state = {
		expanded: null
	};

	handleChange = (panel) => (event, expanded) => {
		this.setState({
			expanded: expanded ? panel : false
		});
	};

	render() {
		const { classes, todos } = this.props;
		const { expanded } = this.state;
		return (
			<Fragment>
				<Grid container spacing={24}>
					<Grid item xs>
						<div className={classes.root}>
							{todos.map((todo) => (
								<ExpansionPanel
									expanded={expanded === todo.id}
									onChange={this.handleChange(todo.id)}
									key={todo.id}
									className={classes.panelHover}
								>
									<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
										<Checkbox
											checked={todo.completed}
											className={classes.checkboxHeading}
											onChange={() => this.props.doneUndoneTodo(todo.id)}
										/>
										<IconButton
											aria-label="Delete"
											className={classes.iconButtonHeading}
											onClick={() => this.props.deleteTodo(todo.id)}
										>
											<DeleteIcon />
										</IconButton>
										<Typography className={classes.heading} align="left">
											{todo.selectedDateString}
										</Typography>

										<Typography noWrap className={classes.secondaryHeading}>
											{todo.title}
										</Typography>
									</ExpansionPanelSummary>
									<ExpansionPanelDetails>
										<Typography>{todo.description}</Typography>
									</ExpansionPanelDetails>
								</ExpansionPanel>
							))}
						</div>
					</Grid>
				</Grid>
			</Fragment>
		);
	}
}

const mapStateToProps = (state) => {
	return { todos: state.todoReducer.todos };
};

const mapDispatchToProps = (dispatch) => {
	return {
		doneUndoneTodo: (todo) => dispatch(doneUndoneTodo(todo)),
		deleteTodo: (todo) => dispatch(deleteTodo(todo))
	};
};

const Lista = connect(mapStateToProps, mapDispatchToProps)(ConnectedList);

Lista.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Lista);
