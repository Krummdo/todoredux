import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import uuidv1 from 'uuid';
import { addMoneyMovement } from '../actions/moneyMovementAction';
import TextField from '@material-ui/core/TextField';
import NumberFormat from 'react-number-format';
import { DatePicker } from 'material-ui-pickers';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';

const styles = (theme) => ({
	root: {
		flexGrow: 1,
		margin: theme.spacing.unit * 2
	},
	button: {
		marginTop: theme.spacing.unit * 2
	}
});

function NumberFormatCustom(props) {
	const { inputRef, onChange, ...other } = props;

	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={(values) => {
				onChange({
					target: {
						value: values.value
					}
				});
			}}
			thousandSeparator
			prefix="$"
		/>
	);
}

NumberFormatCustom.propTypes = {
	inputRef: PropTypes.func.isRequired,
	onChange: PropTypes.func.isRequired
};

class ConectedFormMoneyMovements extends Component {
	constructor() {
		super();
		this.state = {
			selectedDate: new Date(),
			selectedDateString: new Date().toLocaleDateString(),
			description: '',
			amount: '',
			income: 0,
			outcome: 0
		};
	}

	handleChange = (event) => {
		this.setState({ [event.target.id]: event.target.value });
	};

	handleChangeNumber = (name) => (event) => {
		this.setState({
			[name]: event.target.value
		});
	};

	handleSubmit = (valueType) => {
		const { selectedDateString, description, amount } = this.state;
		let { income, outcome } = this.state;
		valueType === 'INCOME' ? (income = amount) : (outcome = amount);
		const id = uuidv1();

		this.props.addMoneyMovement({ id, selectedDateString, description, income, outcome });
		this.setState({
			selectedDateString: new Date().toLocaleDateString(),
			selectedDate: new Date(),
			description: '',
			amount: '',
			income: 0,
			outcome: 0
		});
	};

	handleDateChange = (date) => {
		this.setState({ selectedDateString: date.toLocaleDateString(), selectedDate: date });
	};

	render() {
		const { classes } = this.props;
		const { amount } = this.state;

		return (
			<div className={classes.root}>
				<form className={classes.container} noValidate autoComplete="off" onSubmit={this.handleSubmit}>
					<Grid container spacing={24}>
						<Grid item xs>
							<MuiPickersUtilsProvider utils={DateFnsUtils}>
								<div className="picker">
									<DatePicker
										keyboard
										label="Date"
										format="dd/MM/yyyy"
										placeholder="10/10/2018"
										mask={(value) =>
											value ? [ /\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/ ] : []}
										value={this.state.selectedDate}
										onChange={this.handleDateChange}
										disableOpenOnEnter
										animateYearScrolling={false}
										margin="normal"
										variant="filled"
									/>
								</div>
							</MuiPickersUtilsProvider>
						</Grid>
						<Grid item xs={9}>
							<TextField
								id="description"
								label="Description"
								className={classes.textField}
								margin="normal"
								variant="filled"
								value={this.state.description}
								onChange={this.handleChange}
								fullWidth
							/>
						</Grid>
					</Grid>
					<Grid container spacing={24} direction="row" justify="center" alignItems="flex-start">
						<Grid container item xs={3} justify="flex-start" alignItems="flex-start">
							<TextField
								className={classes.formControl}
								label="Amount"
								value={amount}
								onChange={this.handleChangeNumber('amount')}
								id="formatted-numberformat-input"
								InputProps={{
									inputComponent: NumberFormatCustom
								}}
								margin="normal"
								variant="filled"
							/>
						</Grid>
						<Grid item xs />
						<Grid item xs={2}>
							<Button
								variant="contained"
								color="primary"
								className={classes.button}
								fullWidth
								onClick={() => this.handleSubmit('INCOME')}
							>
								INCOME
							</Button>
							<Button
								variant="contained"
								color="primary"
								className={classes.button}
								fullWidth
								onClick={() => this.handleSubmit('OUTCOME')}
							>
								OUTCOME
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		addMoneyMovement: (moneyMovement) => dispatch(addMoneyMovement(moneyMovement))
	};
};

const FormMoneyMovements = connect(null, mapDispatchToProps)(ConectedFormMoneyMovements);

FormMoneyMovements.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FormMoneyMovements);
