import React, { Component } from 'react';
import { connect } from 'react-redux';
import uuidv1 from 'uuid';
import { addTodo } from '../actions/todoAction';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import { DatePicker } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';

const styles = (theme) => ({
	root: {
		flexGrow: 1,
		margin: theme.spacing.unit * 2
	},
	button: {
		marginTop: theme.spacing.unit * 2
	}
});

class ConnectedFormTodo extends Component {
	constructor() {
		super();
		this.state = {
			title: '',
			description: '',
			selectedDate: new Date(),
			selectedDateString: new Date().toLocaleDateString()
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleDateChange = this.handleDateChange.bind(this);
	}

	handleChange = (event) => {
		this.setState({ [event.target.id]: event.target.value });
	};

	handleSubmit = (event) => {
		event.preventDefault();
		const { selectedDateString, description, title } = this.state;
		const id = uuidv1();
		const completed = false;

		this.props.addTodo({ completed, selectedDateString, description, title, id });
		this.setState({
			selectedDateString: new Date().toLocaleDateString(),
			selectedDate: new Date(),
			description: '',
			title: ''
		});
	};

	handleDateChange = (date) => {
		this.setState({ selectedDateString: date.toLocaleDateString(), selectedDate: date });
	};

	render() {
		const { classes } = this.props;

		return (
			<div className={classes.root}>
				<form className={classes.container} noValidate autoComplete="off" onSubmit={this.handleSubmit}>
					<Grid container spacing={24}>
						<Grid item xs>
							<MuiPickersUtilsProvider utils={DateFnsUtils}>
								<div className="picker">
									<DatePicker
										keyboard
										label="Date"
										format="dd/MM/yyyy"
										placeholder="10/10/2018"
										mask={(value) =>
											value ? [ /\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/ ] : []}
										value={this.state.selectedDate}
										onChange={this.handleDateChange}
										disableOpenOnEnter
										animateYearScrolling={false}
										margin="normal"
										variant="filled"
									/>
								</div>
							</MuiPickersUtilsProvider>
						</Grid>
						<Grid item xs={9}>
							<TextField
								id="title"
								label="Task"
								className={classes.textField}
								margin="normal"
								variant="filled"
								value={this.state.title}
								onChange={this.handleChange}
								fullWidth
							/>
						</Grid>
					</Grid>
					<Grid container spacing={24} direction="row" justify="center" alignItems="flex-start">
						<Grid item xs={10}>
							<TextField
								id="description"
								label="Description"
								multiline
								rows="4"
								value={this.state.description}
								onChange={this.handleChange}
								className={classes.textField}
								margin="normal"
								variant="filled"
								fullWidth
							/>
						</Grid>
						<Grid item xs>
							<Button
								variant="contained"
								color="primary"
								className={classes.button}
								type="submit"
								fullWidth
							>
								ADD
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		addTodo: (todo) => dispatch(addTodo(todo))
	};
};

const FormTodo = connect(null, mapDispatchToProps)(ConnectedFormTodo);

FormTodo.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FormTodo);
