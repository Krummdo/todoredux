import { ADD_TODO, DONE_UNDONE_TODO, DELETE_TODO } from '../constants/todo_action_types';

const initialState = {
	todos: []
};

const todoReducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_TODO:
			return { ...state, todos: [ ...state.todos, action.payload ] };
		case DONE_UNDONE_TODO:
			return Object.assign({}, state, {
				todos: state.todos.map((todo) => {
					if (todo.id === action.payload) {
						return Object.assign({}, todo, {
							completed: !todo.completed
						});
					}
					return todo;
				})
			});
		case DELETE_TODO:
			return Object.assign({}, state, {
				todos: state.todos.filter((todo) => todo.id !== action.payload)
			});

		default:
			return state;
	}
};

export default todoReducer;
