import { ADD_MONEY_MOVEMENT } from '../constants/moneyMovements_action_types';

const initialState = {
	moneyMovements: []
};

const moneyMovementsReducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_MONEY_MOVEMENT:
			return { ...state, moneyMovements: [ ...state.moneyMovements, action.payload ] };
		default:
			return state;
	}
};

export default moneyMovementsReducer;
