import { combineReducers } from 'redux';
import todoReducer from './todoReducer';
import moneyMovementsReducer from './moneyMovementsReducer';

export default combineReducers({
	todoReducer,
	moneyMovementsReducer
});
